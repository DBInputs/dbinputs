# Results

The **Results** folder contains

- the logs resulting from the 10 runs of ABT-DBInputs (equal, disjoint, and overlapping configurations), ABT-Link, and ABT-Random over the subjects found in the **Subjects** folder. At the end of each log, a section named *"# of OK/KO Functionalities:"* lists all the forms visited with valid (OK) or invalid (KO) input data for the considered app and technique.
- the R scripts used to generate the plots in the paper and to compute Wilcoxon test from the enclosed "plot2_data.csv" dataset and the other data extracted from the logs. Please change the path to plot2_data.csv in the R scripts according to your machine.

### Notes

- Due to a non disclosure agreement, the logs for Budgets application are not included in this folder.
- Since we had no access to the Tricentis runtime database, the only possible ABT-DBInputs configuration was the disjoint one, based on an independently (plausible) designed working copy database.
