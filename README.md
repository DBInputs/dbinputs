# DBInputs

A key challenge in automatic Web testing is the generation of syntactically and semantically valid input values that can exercise the many functionalities that impose constraints on the validity of the inputs. Since relying on manually curated catalogs of values is generally too expensive for most practical applications, it is important to automate the input generation process.

This repository hosts the **tools**, **subjects**, and **results** obtained with **DBInputs**, a novel approach that reuses the data from the database of the target Web applications to automatically identify domain-specific and application-specific inputs, and effectively fulfil the validity constraints present in the tested Web pages. To extract valid inputs from the application databases, DBInputs exploits the syntactic and semantic similarity between the identifiers of the input fields and the ones in the tables of the database, automatically resolving the mismatch between the user interface and the schema of the database.

The set of tools available in this repository consists of the AutoBlackTest test generation augmented with DBInputs, and also AutoBlackTest augmented with Link and Radom input generation algorithms for sake of comparison.

The repository is organized in three main folders:

- Tools*
- Subjects
- Results

The **Tools** folder contains all the executables and scripts related to DBInputs.

The **Subjects** folder contains the applications used in the experiments performed to validate DBInputs and their databases when available.

The **Results** folder contains the results obtained by applying DBInputs (using the tools in the **Tools** folder) to the applications in the **Subjects** folder.

*\* Due to the size of some files, the archives are tracked using git **LFS** (Large File Storage).*
