# Tools

The **Tools** folder contains

- ABT-DBInputs tool
- ABT-Link tool
- ABT dependencies
  - BrowserDriver: it contains the ChromeDriver (v. xx) needed to run ABT on a Chrome browser
  - Filters: it contains Dolibarr, Tricentis, and Mantis XML filtering files to choose the elements to consider/ignore during the explation. the default configuration is the one used for our experiments
  - Lemmatizer: it containts the lemmatizer resource needed to syntactic words comparisons
  - Word2Vec: it contains the word2vec model needed to semantic words comparisons (must be unzipped)
- settings file (to configure and run the tools)
  - dataSource: the SQL server where the working copy databases are placed. The LINK version of this file is identical, but requires "Link" as dataSource.
  - username and password: credentials to access dataSource
  - initialCatalog: the name of the working copy database (e.g., dolibarr_equals if the app to run is Dolibarr and we want the same working copy as the runtime DB)
  - application: the url of the app to test (e.g., <http://localhost/mantisbt/login_page.php)>
- lemmatizer: the path to the lemmatizer resource used for syntactic comparison (e.g., ...)
  - browserDriverPath: the path to chrome driver folder (e.g., ...)
  - scanningWidgetsFilter: the path to the filter file used to ignore/consider web elements within pages (e.g., ...)
  - word2vecModel: the path to the word2vec model used for semantics comparisons
  - outputPath: path to ABT output (e.g., C:\outputs). it must be created before the first execution
  
all the other parameters must be kept unaltered to run the tools with the parameters tested during the expertiments

ABT-DBInputs tools are developed and tested on Windows machines.

To launch **ABT-DBInputs** from a shell opened in the ABT-DBInputs folder, run

    .\exe\ABT_Selenium.exe .\settings.config

To launch **ABT-Link** from a shell opened in the ABT-Link folder, run

    .\exe\ABT_Selenium.exe .\settings_Link_example.config

To launch **ABT-Random** any of the previous commands works, but the field *initialCatalog* and *dataSource* in the settings file have to be set to the empty string.
