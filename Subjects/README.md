# Subjects

The **Subjects** folder contains

- Erp includes Dolibarr app v. 10.0.0 (to unzip), MySQL runtime database, SQL Server working copy databases (disjoint, equal, overlapping)
- Tickets includes Mantis Bug Tracker app v. 2.21.0 (to unzip), MySQL runtime database, SQL Server working copy database (disjoint, equal, overlapping)
- Insurance includes SQL Server working copy database used for Tricentis Vehicle Insurance demo application (disjoint)

**Notes**:

- for our experiments, local installations of Dolibarr/Mantis apps and MySQL settings relied on XAMPP v. 7.3.11 and PhPMyAdmin v. 4.9.1, while SQL Server working copies of the databases
were configured using Microsoft SQL Server Management Studio v. 17.9.1
- Tricentis demo application can be found at <http://sampleapp.tricentis.com/101/>
- since we had no access to Tricentis runtime database, only an independently (plausible) designed working copy database was available
- Budgets application data could not be provided due to non disclosure agreement with the industrial partner

**Installation/Execution procedure**:

1. to install Dolibarr/Mantis apps using XAMPP, place the content of the zip under "htdocs" folder of your XAMPP installation path, then follow the apps instructions to install them
2. after the installation of Dolibarr/Mantis apps, and after each run, the initial database has to be reset by importing the associated MySQL runtime database file
    - after importing Dolibarr runtime database, only the modules used in our experiments (i.e., Third Party, Invoices and Payments, Products) will be enabled
3. before running the first experiment, if ABT-DBInputs technique is adopted, import (attach command) into Microsoft SQL Server Management Studio the needed working copy databases
4. run ABT-DBInputs, ABT-Link or ABT-Random tool with a properly configured settings file
